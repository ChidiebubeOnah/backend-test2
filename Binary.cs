﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Backend_task_Task_2
{
    public class Binary
    {
        private  static List<char> _binary;
        private static string _prefix;

        private static bool DecimalDigit(char digit)
        {
            return digit > '0' && digit > '1';
        }

        
        public static void Validate(string binary)
        {
            _binary = binary.ToList();
            bool incorrectFormat = _binary.Exists(DecimalDigit);

            if (incorrectFormat)
            {
                Console.WriteLine("The Input Supplied is not in binary form!");
                return;
            }

            int ones = _binary.Count(x => x.Equals('1'));
            int zeros = _binary.Count(x => x.Equals('0'));

           
            if (ones == zeros)
            {
                foreach (var digit in _binary)
                {
                    _prefix += digit;

                    if (!(_prefix.Count(x => x.Equals('1')) >= _prefix.Count(x => x.Equals('0'))))
                    {
                        Console.WriteLine("Not A good binary string");
                        return;
                    }
                    
                }
                Console.WriteLine("A good binary string");
            }
            else
            {
                Console.WriteLine("Not A good binary string");
            }

        }
    }
}